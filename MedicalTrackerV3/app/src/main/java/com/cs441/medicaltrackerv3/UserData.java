package com.cs441.medicaltrackerv3;

public class UserData {
    private String userID;
    private String email;
    private String phoneNumber;

    public UserData(String userID, String email, String phoneNumber) {
        this.userID = userID;
        this.email = email;
        this.phoneNumber = phoneNumber;
    }

    public UserData(){

    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getUserID() {
        return userID;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }
}
