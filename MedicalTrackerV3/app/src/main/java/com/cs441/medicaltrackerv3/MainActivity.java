package com.cs441.medicaltrackerv3;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity {

    private String TAG;

    private TextInputLayout emailLogin, passwordLogin;
    private Button signUpButton, loginButton;
    private ProgressBar progressBar;
    private FirebaseAuth mAuth;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        signUpButton = (Button) findViewById(R.id.sign_up_button);
        loginButton = findViewById(R.id.login_button);
        emailLogin = findViewById(R.id.email_login);
        passwordLogin = findViewById(R.id.password_login);
        progressBar = findViewById(R.id.progress_bar);
        mAuth = FirebaseAuth.getInstance();

        RelativeLayout mainLayout = (RelativeLayout)findViewById(R.id.activity_rlayout_main);
        mainLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                View focusedView = MainActivity.this.getCurrentFocus();
                InputMethodManager inputMethodManager = (InputMethodManager) MainActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                if (focusedView != null) {
                    inputMethodManager.hideSoftInputFromWindow(focusedView.getWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);
                    return false;
                }
                else{
                    return true;
                }
            }
        });

        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SignUpActivity.class);
                startActivity(intent);
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!confirmAllEntries()){
                    return;
                }
                else{
                    final String email = emailLogin.getEditText().getText().toString();
                    final String password = passwordLogin.getEditText().getText().toString();
                    login(email, password);
                }
            }
        });
    }

    private void login(String email, String password){
        progressBar.setVisibility(View.VISIBLE);
        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                progressBar.setVisibility(View.GONE);
                if(task.isSuccessful()){
                    Intent intent = new Intent(MainActivity.this, MapsActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                }
                else{
                    Toast.makeText(MainActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void openMap(View view)
    {
        Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);
    }

    private boolean validateEmail(){
        String emailInput = emailLogin.getEditText().getText().toString().trim();
        Log.d(TAG, "LKJAHSDLHAS" + emailInput);

        if(emailInput.isEmpty()){
            emailLogin.setError("Email cannot be empty!");
            return false;
        }
        else{
            emailLogin.setError(null);
            return true;
        }
    }

    private boolean validatePassword(){
        String passwordInput1 = passwordLogin.getEditText().getText().toString().trim();
        if(passwordInput1.isEmpty()){
            passwordLogin.setError("Password cannot be empty!");
            return false;
        }
        else{
            passwordLogin.setError(null);
            return true;
        }
    }

    private boolean confirmAllEntries(){
        if(!validateEmail() | !validatePassword()){
            return false;
        }
        else{
            Log.d(TAG, "Registration Info: " + emailLogin.getEditText().getText().toString());
            Log.d(TAG, "Registration Info: " + passwordLogin.getEditText().getText().toString());
            Toast.makeText(this, "Welcome Back!", Toast.LENGTH_SHORT);
            return true;
        }
    }
}
