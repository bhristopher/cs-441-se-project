package com.cs441.medicaltrackerv3;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataParser {

    private HashMap<String, String> getPlace(JSONObject googlePlaceJson)
    {
        HashMap<String, String> googlePlaceMap = new HashMap<>();
        String placeName = "--NA--";
        String vicinity= "--NA--";
        String latitude= "";
        String longitude="";
        String reference="";
        String rating ="";
        String businessStatus = "";
        String userRatingsTotal = "";
        String placeID = "";

        Log.d("from DataParser:"," jsonobject = " + googlePlaceJson.toString());

        try {
            if (!googlePlaceJson.isNull("name")) {
                placeName = googlePlaceJson.getString("name");
            }
            if (!googlePlaceJson.isNull("vicinity")) {
                vicinity = googlePlaceJson.getString("vicinity");
            }
            if (!googlePlaceJson.isNull("rating")){
                rating = googlePlaceJson.getString("rating");
            }
            if(!googlePlaceJson.isNull("business_status")){
                businessStatus = googlePlaceJson.getString("business_status");
            }
            if(!googlePlaceJson.isNull("user_ratings_total")){
                userRatingsTotal = googlePlaceJson.getString("user_ratings_total");
            }
            if(!googlePlaceJson.isNull("place_id")){
                placeID = googlePlaceJson.getString("place_id");
            }

            latitude = googlePlaceJson.getJSONObject("geometry").getJSONObject("location").getString("lat");
            longitude = googlePlaceJson.getJSONObject("geometry").getJSONObject("location").getString("lng");
            reference = googlePlaceJson.getString("reference");

            //Store key -> value into HashMap
            googlePlaceMap.put("place_name", placeName);
            googlePlaceMap.put("vicinity", vicinity);
            googlePlaceMap.put("lat", latitude);
            googlePlaceMap.put("lng", longitude);
            googlePlaceMap.put("reference", reference);
            googlePlaceMap.put("place_id", placeID);
            googlePlaceMap.put("rating", rating);
            googlePlaceMap.put("business_status", businessStatus);
            googlePlaceMap.put("user_ratings_total", userRatingsTotal);

//            jsonobject = {"business_status":"OPERATIONAL",
//                    "geometry":{"location":{"lat":32.93677880000001,"lng":-117.1093871},"viewport":{"northeast":{"lat":32.9381644802915,"lng":-117.1079460697085},"southwest":{"lat":32.93546651970851,"lng":-117.1106440302915}}},
//                    "icon":"https:\/\/maps.gstatic.com\/mapfiles\/place_api\/icons\/generic_business-71.png",
//                    "id":"37130c7787ab3efb34e9e6ad5da54f4339c9bc02",
//                    "name":"Children's Primary Care Medical Group Scripps Ranch",
//                    "opening_hours":{"open_now":true},
//                    "photos":[{"height":600,"html_attributions":["<a href=\"https:\/\/maps.google.com\/maps\/contrib\/104834498302674354401\">Children&#39;s Primary Care Medical Group Scripps Ranch<\/a>"],
//                    "photo_reference":"CmRaAAAATmkNYVmJSU6NGoOSjF2amYjiQ2qpa44FHTC5TEa3bMZeOHV0J-6g1fnjPvJHQFs0wA8OuqziYMAZLXU_rQcsk1ehMrpLMJrRudnMRyffG9Evm0wSZR67L1hBV7wgWYwsEhB2c6wWTOV3s7f6gOnmdmQoGhSVrNiZA6sp2FLqQZJWYEEKFiRLNw","width":1080}],
//                    "place_id":"ChIJ7Vz-SKf524ARFs1uVm6zkwE",
//                    "plus_code":{"compound_code":"WVPR+P6 San Diego, California, United States",
//                    "global_code":"8544WVPR+P6"},
//                    "rating":4.4,
//                    "reference":"ChIJ7Vz-SKf524ARFs1uVm6zkwE",
//                    "scope":"GOOGLE",
//                    "types":["hospital","doctor","health","point_of_interest","establishment"],
//                    "user_ratings_total":11,
//                    "vicinity":"12036 Scripps Highlands Drive, San Diego"}
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        return googlePlaceMap;

    }
    private List<HashMap<String, String>>getPlaces(JSONArray jsonArray)
    {
        int count = jsonArray.length();
        List<HashMap<String, String>> placelist = new ArrayList<>();
        HashMap<String, String> placeMap = null;

        for(int i = 0; i<count;i++)
        {
            try {
                placeMap = getPlace((JSONObject) jsonArray.get(i));
                placelist.add(placeMap);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return placelist;
    }

    public List<HashMap<String, String>> parse(String jsonData)
    {
        JSONArray jsonArray = null;
        JSONObject jsonObject;

        Log.d("From parse function:", jsonData);

        try {
            jsonObject = new JSONObject(jsonData);
            jsonArray = jsonObject.getJSONArray("results");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return getPlaces(jsonArray);
    }
}
