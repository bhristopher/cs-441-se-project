package com.cs441.medicaltrackerv3;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class SignUpActivity extends AppCompatActivity {

    private String TAG;
    private FirebaseAuth mAuth;
    private TextInputLayout emailAddress, passwordID1, passwordID2, phoneNumber;
    private Button signUpButton;
    private ProgressBar progressBar;
    private DatabaseReference databaseReference;


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Register");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mAuth = FirebaseAuth.getInstance();

        emailAddress = findViewById(R.id.email_id);
        passwordID1 = findViewById(R.id.password_id_1);
        passwordID2 = findViewById(R.id.password_id_2);
        phoneNumber = findViewById(R.id.phone_number);
        signUpButton = findViewById(R.id.sign_up_button);
        progressBar = findViewById(R.id.progress_bar);

        RelativeLayout signLayout = (RelativeLayout)findViewById(R.id.activity_rlayout_sign);
        signLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                View focusedView = SignUpActivity.this.getCurrentFocus();
                InputMethodManager inputMethodManager = (InputMethodManager) SignUpActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                if (focusedView != null) {
                    inputMethodManager.hideSoftInputFromWindow(focusedView.getWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);
                    return false;
                }
                else{
                    return true;
                }
            }
        });

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SignUpActivity.this, MainActivity.class));
            }
        });

        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!confirmAllEntries()){
                    Log.d(TAG, "Registration Info: " + emailAddress.getEditText().getText().toString());
                    Log.d(TAG, "Registration Info: " + passwordID1.getEditText().getText().toString());
                    Log.d(TAG, "Registration Info: " + passwordID2.getEditText().getText().toString());
                    Log.d(TAG, "Registration Info: " + phoneNumber.getEditText().getText().toString());
                    return;
                }
                else{
                    final String email = emailAddress.getEditText().getText().toString();
                    final String password = passwordID2.getEditText().getText().toString();
                    final String phoneNum = phoneNumber.getEditText().getText().toString();
                    register(email, password, phoneNum);
                }
            }
        });
    }

    private boolean validateEmail(){
        String emailInput = emailAddress.getEditText().getText().toString().trim();

        if(emailInput.isEmpty()){
            emailAddress.setError("Email cannot be empty!");
            return false;
        }
        else{
            emailAddress.setError(null);
            return true;
        }
    }

    private boolean validatePassword1(){
        String passwordInput1 = passwordID1.getEditText().getText().toString().trim();
        if(passwordInput1.isEmpty()){
            passwordID1.setError("Password cannot be empty!");
            return false;
        }
        else{
            passwordID1.setError(null);
            return true;
        }
    }

    private boolean validatePassword2(){
        String passwordInput1 = passwordID1.getEditText().getText().toString();
        String passwordInput2 = passwordID2.getEditText().getText().toString();
        if(passwordInput2.isEmpty()){
            passwordID2.setError("Password 2 is empty");
            return false;
        }
        else if(!passwordInput1.equals(passwordInput2)){
            passwordID1.setError("Passwords do not match!");
            passwordID1.setError("Passwords do not match!");
            return false;
        }
        else{
            passwordID2.setError(null);
            return true;
        }
    }

    private boolean validatePhoneNumber(){
        String phoneNum = phoneNumber.getEditText().getText().toString().trim();
        if(phoneNum.isEmpty()){
            phoneNumber.setError("Phone number cannot be empty!");
            return false;
        }
        else{
            phoneNumber.setError(null);
            return true;
        }
    }

    private boolean confirmAllEntries(){
        if(!validateEmail() | !validatePassword1() | !validatePassword2() | !validatePhoneNumber()){
            Log.d(TAG, "Registration Info: " + emailAddress.getEditText().getText().toString());
            Log.d(TAG, "Registration Info: " + passwordID1.getEditText().getText().toString());
            Log.d(TAG, "Registration Info: " + passwordID2.getEditText().getText().toString());
            Log.d(TAG, "Registration Info: " + phoneNumber.getEditText().getText().toString());
            return false;
        }
        else{
            Log.d(TAG, "Registration Info: " + emailAddress.getEditText().getText().toString());
            Log.d(TAG, "Registration Info: " + passwordID2.getEditText().getText().toString());
            Log.d(TAG, "Registration Info: " + phoneNumber.getEditText().getText().toString());
            Toast.makeText(this, "Registration Complete!", Toast.LENGTH_SHORT);
            return true;
        }
    }

    private void register(final String email, final String password, final String phoneNumber){
        progressBar.setVisibility(View.VISIBLE);
        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    FirebaseUser user = mAuth.getCurrentUser();
                    String userID = user.getUid();
                    databaseReference = FirebaseDatabase.getInstance().getReference("Users").child(userID);
                    HashMap<String, String> hashMap = new HashMap<>();
                    hashMap.put("userID", userID);
                    hashMap.put("email", email);
                    hashMap.put("userID", phoneNumber);
                    databaseReference.setValue(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            progressBar.setVisibility(View.GONE);
                            if(task.isSuccessful()){
                                Intent intent = new Intent(SignUpActivity.this, MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                            else{
                                Log.d(TAG, "THIS IS CALLED HERE");
                                Toast.makeText(SignUpActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
                else{
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(SignUpActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
