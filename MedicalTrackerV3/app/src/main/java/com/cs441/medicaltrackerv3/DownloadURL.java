package com.cs441.medicaltrackerv3;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class DownloadURL {

    String TAG;

    public String readUrl(String myUrl) throws IOException
    {
        String data = "";
        InputStream inputStream = null;
        HttpURLConnection urlConnection = null;

        try {
            URL url = new URL(myUrl);
            Log.d(TAG, "From readURL: URL connection attempt ...");
            urlConnection=(HttpURLConnection) url.openConnection();
            urlConnection.connect();
            Log.d(TAG, "From readURL: URL connection established ...");

            inputStream = urlConnection.getInputStream();
            Log.d(TAG, "From readURL: inputStream marked ...");
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
            Log.d(TAG, "From readURL: Buffer Reader marked ...");
            StringBuffer sb = new StringBuffer();

            String line = "";
            while((line = br.readLine()) != null)
            {
                sb.append(line);
            }
            Log.d(TAG, "From readURL: Appending strings ...");

            data = sb.toString();
            br.close();
            Log.d(TAG, "From readURL: Closing Buffered Reader strings ...");

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            inputStream.close();
            Log.d(TAG, "From readURL: Input Stream Closed ...");
            urlConnection.disconnect();
            Log.d(TAG, "From readURL: URL Connection Disconnected ...");
        }
        Log.d(TAG,"From DownloadURL: Returning data ... = " + data);

        return data;
    }
}
