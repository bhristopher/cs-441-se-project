package com.cs441.medicaltrackerv3;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.telephony.PhoneNumberUtils;

import com.google.android.gms.common.util.Strings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.libraries.places.api.model.OpeningHours;
import com.google.android.libraries.places.api.model.PhotoMetadata;
import com.google.android.libraries.places.api.model.PlusCode;

import java.io.Serializable;
import java.util.List;

public class PlaceInfo implements Serializable {

    private String address;
    private String ID;
    private LatLng latLng;
    private String name;
    private OpeningHours openingHours;
    private String phoneNumber;
    private Integer priceLevel;
    private double rating;
    private java.util.List<PhotoMetadata> photoMetadata;
    private Integer totalUserRatings;
    private LatLngBounds viewport;
    private Uri webUri;

    public List <PlaceInfo> placesInfoList;

    public PlaceInfo(String address, String id, LatLng latLng, String name, OpeningHours openingHours,
                     String phoneNumber, Integer priceLevel, Double rating, Integer totalUserRatings,
                     LatLngBounds viewport, Uri webUri, java.util.List<PhotoMetadata> photoMetadata) {
        this.address = address;
        this.ID = ID;
        this.latLng = latLng;
        this.name = name;
        this.openingHours = openingHours;
        this.phoneNumber = phoneNumber;
        this.priceLevel = priceLevel;
        this.rating = rating;
        this.totalUserRatings = totalUserRatings;
        this.viewport = viewport;
        this.webUri = webUri;
        this.photoMetadata = photoMetadata;
    }

    public PlaceInfo() {

    }

    public List<PlaceInfo> getList() {
        return placesInfoList;
    }

    public String getAddressPlace() {
        return address;
    }

    public void setAddressPlace(String address) {
        this.address = address;
    }

    public String getIDPlace() {
        return ID;
    }

    public void setIDPlace(String ID) {
        this.ID = ID;
    }

    public LatLng getLatLngPlace() {
        return latLng;
    }

    public void setLatLngPlace(LatLng latLng) {
        this.latLng = latLng;
    }

    public String getNamePlace() {
        return name;
    }

    public void setNamePlace(String name) {
        this.name = name;
    }

    public OpeningHours getOpeningHoursPlace() {
        return openingHours;
    }

    public void setOpeningHoursPlace(OpeningHours openingHours) { this.openingHours = openingHours; }

    public String getPhoneNumberPlace() {
        return phoneNumber;
    }

    public void setPhoneNumberPlace(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getPriceLevelPlace() {
        return priceLevel;
    }

    public void setPriceLevelPlace(int priceLevel) {
        this.priceLevel = priceLevel;
    }

    public double getRatingPlace() {
        return rating;
    }

    public void setRatingPlace(double rating) {
        this.rating = rating;
    }

    public int getTotalUserRatingsPlace() {
        return totalUserRatings;
    }

    public void setTotalUserRatingsPlace(int totalUserRatings) {
        this.totalUserRatings = totalUserRatings;
    }

    public LatLngBounds getViewportPlace() {
        return viewport;
    }

    public void setViewportPlace(LatLngBounds viewport) {
        this.viewport = viewport;
    }

    public Uri getWebUriPlace() {
        return webUri;
    }

    public void setWebUriPlace(Uri webUri) {
        this.webUri = webUri;
    }

    public java.util.List<PhotoMetadata> getPhotoMetaDataPlace(){
        return photoMetadata;
    }

    public void setPhotoMetaDataPlace(java.util.List<PhotoMetadata> photoMetadata){
        this.photoMetadata = photoMetadata;
    }

    public String toStringPlace() {
        return "PlaceInfo{" +
                "address='" + address + '\'' +
                ", ID='" + ID + '\'' +
                ", latLng=" + latLng +
                ", name='" + name + '\'' +
                ", openingHours=" + openingHours +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", priceLevel=" + priceLevel +
                ", rating=" + rating +
                ", totalUserRatings=" + totalUserRatings +
                ", viewport=" + viewport +
                ", webUri=" + webUri +
                ", photoMetadata=" + photoMetadata +
                '}';
    }
}