package com.cs441.medicaltrackerv3;

import android.content.ContentValues;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Parcelable;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class GetNearbyPlacesData extends AsyncTask<Object, String, String> implements
        Serializable
{
    public ArrayList<ArrayList<String>> placesListofList;

    private String googlePlacesData;
    private GoogleMap mMap;
    private String url;
    private String TAG;

    //HashMap Vars
    private String placeName;
    private String vicinity;
    private float rating;
    private String businessStatus;
    private int totalRatings;
    private double lat;
    private double lng;
    private LatLng latLng;
    private String placeID;

    @Override
    protected String doInBackground(Object... objects){
        Log.d(TAG, "From doInBackground: Acquiring GoogleMap Objects[0] and String Objects[1] ...");
        mMap = (GoogleMap)objects[0];
        url = (String)objects[1];
        Log.d(TAG, "From doInBackground: Complete ...");

        DownloadURL downloadURL = new DownloadURL();
        try {
            Log.d(TAG, "From doInBackground: Download URL ...");
            googlePlacesData = downloadURL.readUrl(url);
            Log.d(TAG, "From doInBackground: Complete ...");
        } catch (IOException e) {
            e.printStackTrace();
        }

        return googlePlacesData;
    }

    @Override
    protected void onPostExecute(String s){
        Log.d(TAG, "From onPostExecute: Hashmap Nearby Place List ...");
        List<HashMap<String, String>> nearbyPlaceList;
        DataParser parser = new DataParser();
        nearbyPlaceList = parser.parse(s);
        Log.d(TAG, "From onPostExecute: Calling Parse Method ...");
        showNearbyPlaces(nearbyPlaceList);
    }

    public void showNearbyPlaces(List<HashMap<String, String>> nearbyPlaceList)
    {
        Log.d(TAG, "From showNearbyPlaces: Showing Nearby Places ...");
        for(int i = 0; i < nearbyPlaceList.size(); i++)
        {
            HashMap<String, String> googlePlace = nearbyPlaceList.get(i);

            Log.d(TAG, "From showNearbyPlaces: Showing Place name and Vicinity and LATLNG... " + placeID);

            placeName = googlePlace.get("place_name");
            vicinity = googlePlace.get("vicinity");

            if(!googlePlace.get("rating").isEmpty()){
                rating = Float.parseFloat(googlePlace.get("rating"));
            }
            if(!googlePlace.get("business_status").isEmpty()){
                businessStatus = googlePlace.get("business_status");
            }
            if(googlePlace.get("total_ratings") != null) {
                totalRatings = Integer.parseInt(googlePlace.get("user_ratings_total"));
            }
//            if(googlePlace.get("place_id") != null){
//                placeID = googlePlace.get("place_id");
//            }
            placeID = googlePlace.get("place_id");

            lat = Double.parseDouble( googlePlace.get("lat"));
            lng = Double.parseDouble( googlePlace.get("lng"));
            latLng = new LatLng(lat, lng);

            Log.d(TAG, "From showNearbyPlaces: Applying Markers ...");

            MarkerOptions markerOptions = new MarkerOptions();

            markerOptions.title(placeName);
            markerOptions.position(latLng);
            markerOptions.snippet(placeID);

            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));

            mMap.addMarker(markerOptions);
//            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(12));

//            ArrayList<String> tempList = new ArrayList<>();
//            tempList.add(placeName);
//            tempList.add(vicinity);
//            tempList.add(googlePlace.get(rating)); //Unparsed string
//            tempList.add(businessStatus);
//            tempList.add(googlePlace.get(totalRatings));    //Unparsed totalRatings
//            tempList.add(googlePlace.get(lat)); //unparsed
//            tempList.add(googlePlace.get(lng)); //Unparsed

            Log.d(TAG, "From showNearbyPlaces: DONE ...");
        }
    }
}
