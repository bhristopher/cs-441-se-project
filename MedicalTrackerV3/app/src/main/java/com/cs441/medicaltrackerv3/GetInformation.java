package com.cs441.medicaltrackerv3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.libraries.places.api.model.PhotoMetadata;
import com.google.android.libraries.places.api.model.Place;

import java.io.Serializable;

public class GetInformation extends AppCompatActivity implements Serializable {

    private String TAG = "Get Information --- ";
    private String placeID;
    private String placeName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_information);

        Bundle bundle = getIntent().getExtras();
        Serializable value  = bundle.getSerializable("some_key");
        Serializable value2 = bundle.getSerializable("some_key2");
        Serializable value3 = bundle.getSerializable("some_key3");
        Parcelable value4 = bundle.getParcelable("some_key4");
        Serializable value5 = bundle.getSerializable("some_key5");
        Parcelable value6 = bundle.getParcelable("some_ley6");
        Serializable value7 = bundle.getSerializable("some_key7");
        Serializable value8 = bundle.getSerializable("some_key8");
        Serializable value9 = bundle.getSerializable("some_key9");
        Serializable value10 = bundle.getSerializable("some_key10");

        Log.d(TAG, "Get name place: " + value);
        Log.d(TAG, "Get ID Place: " + value2);
        Log.d(TAG, "Get address place: "  + value3);
        Log.d(TAG, "Get LatLng: " + value4);
        Log.d(TAG, "Get phone number: : " + value5);
        Log.d(TAG, "getPhotoMetaData " + value6);
        Log.d(TAG, "getPriceLevelPlace: " + value7);
        Log.d(TAG, "getRatingPlace: " + value8);
        Log.d(TAG, "getTypesPlace: " + value9);
        Log.d(TAG, "getTotalUserRatingsPlace: " + value10);
    }
}


//            try{
//                PhotoMetadata photoMetadata = newPlace.getPhotoMetadatas().get(0);
//                String attributions = photoMetadata.getAttributions();
//                final ImageView imageView = (ImageView) findViewById(R.id.metadataPhoto);
//                FetchPhotoRequest photoRequest = FetchPhotoRequest.builder(newPlace.getPhotoMetadatas().get(0)).setMaxWidth(200).setMaxHeight(200).build();
//                placesClient.fetchPhoto(photoRequest).addOnSuccessListener(new OnSuccessListener<FetchPhotoResponse>() {
//                    @Override
//                    public void onSuccess(FetchPhotoResponse fetchPhotoResponse) {
//                        Log.d(TAG, "moveCameraPos: IMAGE META DATA FETCHre: " + fetchPhotoResponse);
//                        Bitmap bitmap = fetchPhotoResponse.getBitmap();
//                        Log.d(TAG, "moveCameraPos: IMAGE META DATA bitmap: " + bitmap);
////                        ImageView imageView = (ImageView) findViewById(R.id.metadataPhoto);
////                        Log.d(TAG, "moveCameraPos: IMAGE META DATA imageView: " + imageView);
//                        imageView.setImageBitmap(bitmap);
//                        Log.d(TAG, "moveCameraPos: IMAGE META DATA imageView2: " + imageView);
//                    }
//                });