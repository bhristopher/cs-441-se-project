package com.cs441.medicaltrackerv3;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.FetchPlaceRequest;
import com.google.android.libraries.places.api.net.FetchPlaceResponse;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.google.gson.annotations.SerializedName;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static android.widget.Toast.*;


public class MapsActivity extends AppCompatActivity implements
        ActivityCompat.OnRequestPermissionsResultCallback,
        Serializable
{

    // Global variable for camera
    private static final float CAMERA_ZOOM = 15f;

    // Global variable for map markers
    private Marker mMarker;

    // For log debugging purposes
    public String TAG = "MapsActivity --- ";

    // Bool for denying or accepting permissions from user
    private boolean mPermissionGranted = false;

    // Google map object
    private GoogleMap mMap;

    // Permissions
    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COARSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;

    private Location currentLocation;

    // Search bar global vars
    private String mSearchText;
    private ImageView mGps;

    // Places variables
    FusedLocationProviderClient client;
    PlacesClient placesClient;
    PlaceInfo mPlace;

    SupportMapFragment supportMapFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        mGps = (ImageView) findViewById(R.id.ic_gps);

        supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        client = LocationServices.getFusedLocationProviderClient(this);
        getLocationPermission();
    }

    private void getLocationPermission(){
        if(ActivityCompat.checkSelfPermission(MapsActivity.this, FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            Log.d(TAG, "getLocationPermission: gets called");
            getCurrentLocation();
        }
        else{
            ActivityCompat.requestPermissions(MapsActivity.this, new String[]{FINE_LOCATION}, 1000);
        }
    }

    private void getCurrentLocation(){
        Log.d(TAG, "getCurrentLocation: gets called");
        Task<Location> task = client.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(final Location location) {
                if(location != null){
                    supportMapFragment.getMapAsync(new OnMapReadyCallback() {
                        @Override
                        public void onMapReady(GoogleMap googleMap) {
                            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                            MarkerOptions options = new MarkerOptions().position(latLng).title("My Location");
                            mMap = googleMap;
                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
                            mMap.setMyLocationEnabled(true);    //Shows the users blue dot on the map
                            mMap.getUiSettings().setMyLocationButtonEnabled(false); // get rid of the return to location button provided by google's API to use our own
                            mMap.getUiSettings().setMapToolbarEnabled(false);
                            initPlaces();
                        }
                    });
                }
                else{
                    //log something
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d(TAG, "onRequestPermissionsResult: gets called");
        if(requestCode == 1000){
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                getCurrentLocation();
            }
        }
    }

    //Create the autocomplete API and get places information
    private void initPlaces(){
        String apiKey = "AIzaSyBr-nlx0AQ-wQiL-gHT2XNBf-bLgxWnAW0";

        if(!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), apiKey);
        }

        placesClient = Places.createClient(this);

        //Create the auto complete fragment and set the return fields that you want, these are all of them at the moment
        final AutocompleteSupportFragment autocompleteSupportFragment = (AutocompleteSupportFragment) getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment);
        assert autocompleteSupportFragment != null;
        autocompleteSupportFragment.setHint("Look for a hospital");
        autocompleteSupportFragment.setPlaceFields(Arrays.asList(
                Place.Field.ADDRESS,
                Place.Field.ADDRESS_COMPONENTS,
                Place.Field.ID,
                Place.Field.LAT_LNG,
                Place.Field.NAME,
                Place.Field.OPENING_HOURS,
                Place.Field.PHONE_NUMBER,
                Place.Field.PHOTO_METADATAS,
                Place.Field.PLUS_CODE,
                Place.Field.PRICE_LEVEL,
                Place.Field.RATING,
                Place.Field.TYPES,
                Place.Field.USER_RATINGS_TOTAL,
                Place.Field.VIEWPORT,
                Place.Field.UTC_OFFSET,
                Place.Field.WEBSITE_URI)); //Specify which data types to return
        //When the user selects a place,
        autocompleteSupportFragment.setOnPlaceSelectedListener(new PlaceSelectionListener()
        {
            // When a user selects an option from the list, locate and pan the camera to the location
            @Override
            public void onPlaceSelected(@NonNull Place place) {
                mMap.clear();
                mSearchText = place.getName();
                PlaceInfo placeObj = new PlaceInfo(place.getAddress(),
                        place.getId(),
                        place.getLatLng(),
                        place.getName(),
                        place.getOpeningHours(),
                        place.getPhoneNumber(),
                        place.getPriceLevel(),
                        place.getRating(),
                        place.getUserRatingsTotal(),
                        place.getViewport(),
                        place.getWebsiteUri(),
                        place.getPhotoMetadatas());

                Log.d(TAG, "Geolocate function has started..." + mSearchText);
                geoLocate(placeObj); // Geolocate the name of the Place
            }

            @Override
            public void onError(@NonNull Status status) {
                Log.d(TAG, "From onPlaceSelected... No cannot find place on null object");
            }
        });

        // When the user clicks the image view location, camera pans back to current location
        mGps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: clicked GPS icon");
                getCurrentLocation();
            }
        });
    }

    private void geoLocate(PlaceInfo newPlace)
    {
        Log.d(TAG, "Geolocate function has started..." + mSearchText);
        Geocoder geocoder = new Geocoder(MapsActivity.this);
        List<Address> list = new ArrayList<>();
        try
        {
            list = geocoder.getFromLocationName(mSearchText, 1); //Change the max number of list of addresses value later
            Log.d(TAG, "Geolocation --- If the list size is bigger than 0... " + mSearchText);
        }
        catch(IOException e)
        {
            Log.e(TAG, "geolocation IOEXCEPTION: " + e.getMessage() );
        }

        if(list.size() > 0)
        {
            if(newPlace.getViewportPlace() != null){
                displayCustomInfoWindow(newPlace);
            }
            else makeText(this, "Destination cannot be found.", LENGTH_SHORT).show();
        }
    }

    private void displayCustomInfoWindow(PlaceInfo newPlace){
        if(newPlace != null){
            try{
                LatLng latLng = newPlace.getLatLngPlace();
                String snippet = newPlace.getAddressPlace() + "\n" +
                        "Phone Number: " + newPlace.getPhoneNumberPlace() + "\n" +
                        "Website: " + newPlace.getWebUriPlace() + "\n" +
                        "Price Rating: " + newPlace.getRatingPlace() + "\n";
                MarkerOptions options = new MarkerOptions()
                        .position(newPlace.getLatLngPlace())
                        .title(newPlace.getNamePlace())
                        .snippet(snippet);
                mMarker = mMap.addMarker(options);
                mMap.setInfoWindowAdapter(new CustomInfoWindowAdapter(MapsActivity.this));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
                mMarker.showInfoWindow(); //Auto show the options

            }catch (NullPointerException e){
                Log.e(TAG, "moveCamera: NullPointerException: " + e.getMessage());
            }
        }

//        LatLng latLng = newPlace.getLatLngPlace();
//        String snippet = "Address: " + newPlace.getAddressPlace() + "\n" +
//                "Phone number: " + newPlace.getPhoneNumberPlace() + "\n" +
//                "Website: " + newPlace.getWebUriPlace()  + "\n" +
//                "Opening Hours: " + newPlace.getOpeningHoursPlace();
//        MarkerOptions options = new MarkerOptions().position(latLng).title(newPlace.getNamePlace()).snippet(snippet);
//        mMarker = mMap.addMarker(options);
//        mMarker.showInfoWindow(); //Auto show the options
//        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
//        //If the user wants to look at all of the info, create new scrollable intent to display
//        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
//            @Override
//            public boolean onMarkerClick(Marker marker) {
//                Intent intent = new Intent(getApplicationContext(), GetInformation.class);
//                startActivity(intent);
//                return false;
//            }
//        });
    }
}